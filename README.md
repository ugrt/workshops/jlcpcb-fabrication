# JLCPCB Fabrication

This is an instructional repo to guide you through preparing a KiCAD project for fabrication from JLCPCB https://jlcpcb.com/HAR.

The PCB used as the example was designed in KiCAD for controlling the lighting systems on our Mars Rover:
![UGRT Light Board](./resources/JLC PCB main.png)

## Motivation

The goal of this project was to design a custom PCB that could drive all the lights required to help our rover see in the dark!

Originally, our lighting control was managed by an Arduino soldered to a perf board, and screw terminals were used to connect all the wires out to the lights. 

When we decided to overhaul our electrical systems, we decided a redesign of this component could help reduce maintenance overhead since hooking up dozens of wires to screw terminals can be very monotonous. Additionally, since we had started to implement CAN Bus communication to integrate the rest of the rover's electrical subsystems, such as the steering and drive system, as well as the robotic arm, adding the light system to that list was an easy choice that allows us to use CAN Bus as the central communications hub for all control tasks.

This article will go through some of the engineering design decisions that were made in the development of this board, and will walk through the entire process from schematic capture all the way through to ordering the final product from JLCPCB. 

Thanks so much to JLCPCB for sponsoring the fabrication of these boards! JLCPCB provides rapid production of high-quality custom PCBs at very affordable prices, and with the best customer service. If you are looking to fabricate a small or large batch of PCBs for your next project, take a look at the capabilities JLCPCB has to offer!

https://jlcpcb.com/HAR

## Tutorial
### Preparing project for export
JLCPCB offers custom PCB fabrication and SMT assembly services at https://jlcpcb.com/HAR!

SMT soldering can be fun, but it requires some special tools, it takes practice to do well, and it can be very frustrating along the way! 

We decided to use JLCPCB's SMT assembly service for these boards to make things as easy for us as possible. The following steps outline the process to setup your KiCAD project so that you can export all the proper files such as the footprint position (.pos) file, Bill of Materials (BOM), and of course, the all important drill and gerber files.

#### Step 1: Creating BOM

First, you must navigate to Edit Symbol Fields (1) from within KiCAD's schematic window, and add a new field (2) called **_LCSC Part #_** (3). Then you just have to add JLCPCB part numbers for each of the components you would like to have assembled for you (4):
![Add LCSC Part #s](./resources/Add_LCSC_Parts.png)

Most of the components on this board are actually through hole (THT) and not surface mount (SMT) since it is mostly connectors. We will have to add these parts once we receive the boards as JLC's assembly service currently only supports SMT devices. That is okay because THT soldering is much easier and faster than SMT soldering and we can still save time by using JLC's SMT assembly for all of the SMT components we need.

[JLC's parts database](https://jlcpcb.com/parts) is simple to use once you know what parts you are looking for. For example, finding the part number for the 0.1uF capacitors above (4), is done by searching for something like **_100nF 0603_**. We usually check the Basic parts box as shown below since these are the easiest parts to source, and avoid potential surcharges:
![JLCPCB Parts Example](./resources/LCSC example.png)

Once you have input part numbers for all of your SMT components into KiCAD, you are ready to generate a BOM.

As seen in the screenshot below, start by clicking on the BOM icon in KiCAD's schematic window (1). There is a handy script available to download from [arturo182](https://gist.github.com/arturo182/a8c4a4b96907cfccf616a1edb59d0389)'s GitHub that can be added into KiCAD as a plugin (2), which will automatically generate a BOM following the conventions that JLC specifies [on their website](https://support.jlcpcb.com/article/84-how-to-generate-the-bom-and-centroid-file-from-kicad). Once added, you should be able to click Generate to create the BOM in .csv format (3):
![Create BOM](./resources/Creating BOM.png)

I personally had issues running the script within KiCAD, but was able to execute the script from the command line. Judging from some comments on the GitHub, this may have been due to spaces within my KiCAD project path, but YMMV.

With the .csv BOM file generated into your project folder, create a new folder called **_assembly_** inside the root project directory and move the BOM file inside this folder. I also like to append a -BOM suffix to this file. This just keeps the project directory better organized.

#### Step 2: Generate Footprint Position File

Next you will need to generate a footprint positions file so that JLC's Pick and Place machine knows what orientation each of your parts need to be placed. This is crucial for polar components like electrolytic capacitors, or integrated circuits, while others like resistors and ceramic capacitors don't matter if they are placed one way or another, so long as they are placed on the pads!

You must have the KiCAD layout window open to generate this file. Navigate to _File > Fabrication Outputs > Footprint Position (.pos) File_, then set the Output directory to the **_assembly_** folder we just made (1), set the parameters as shown (2), and finally click **_Generate Position File_** as seen in the screenshots below:
![Genreate .pos File](./resources/generate pos file.png)

To keep the files better organized, I like to rename this file with the suffix _-Footprint-Positions_ rather than _-all-pos_, just to make the file name more meaningful. You need to open this file and rename the headings to comply with JLC's requirements. The screenshot below shows the proper header names after changing them in Excel:

![Changes to .pos File](./resources/excel.png)

#### Step 3: Generate Gerber Files

Finally, we are ready to generate the gerber (.gbr) and drill (.drl) files. 

First, we should create another new folder in the project root called **_gerber_**.

Then, again from the KiCAD layout window, navigate to _File > Plot_. In the window that appears, set the Output directory to the **_gerber_** folder we just made (1), click _Generate Drill Files_ (2), confirm the default drill file parameters (3), click _Generate Drill File_ (4) then close the Generate Drill Files window, confirm the default gerber parameters (5), and finally click _Plot_ (6), as shown in the screenshots below:
![Generate Gerber](./resources/generate gerber.png)

Lastly, you need to zip the contents of the gerber folder into a .zip file before you can move to the next stage of uploading to JLC's website.

### Uploading to JLCPCB
Now comes the fun part! Head over to https://jlcpcb.com/HAR and click on Upload gerber file, as shown below:
![JLC Upload Gerber](./resources/JLC upload gerber.png)

Once uploaded, you can make changes to the default settings applied for your PCB, based on the gerber files. 

The only things we changed are the PCB colour to black (the new purple colour would undoubtedly look awesome, but unfortunately it is not one of our school colours!), the surface finish to the lead free HASL option, and we have also chosen to specify the order number on the backside of our PCBs by placing the text **JLCJLCJLCJLC** on the rear silkscreen layer. These changes are indicated in the screenshot below:
![JLC PCB Settings](./resources/JLC PCB settings.png)

Next, we need to set our parameters for the SMT Assembly service. As seen below, we only chose to assemble 2 of the 5 boards we have ordered. We really only need one of these boards but 5 is the minimum quantity of PCBs you can order, and 2 is the minimum quantity that you can have assembled. There is also an option to specify where the tooling holes will be placed, but we chose to leave this up to the engineers at JLC since this board is not very crowded and we are not picky.
![JLC SMT Settings](./resources/JLC SMT settings.png)

Next, after uploading both the BOM and footprint placer files, you will reach the following screen where you can verify that the JLCPCB part numbers you entered into KiCAD match the components that will be assembled:
![JLC Verify Components](./resources/JLC Select parts.png)

After verifying the part numbers, you are ready to move on to the next stage of verifying the components' orientations.

### Verify Component Placement
The last step before you are ready to order your PCBs is checking the orientation of your assembled components. 

As seen in the screenshot below, the JLC website gives you a preview of your PCB which you can zoom and move around in order to check how it thinks certain parts should be oriented:
![JLC Final Check](./resources/Final check.png)

The red boxes indicate components that are not going to be assembled by JLC. In this case, JLC did not have the diode and MOSFET IC that we wanted and so we opted to exclude those components from the assembly. This is done simply by not specifying a JLCPCB part number in the BOM file.

As for the parts that _will_ be assembled, there are no components for which orientation matters and so there is nothing much for us to check here. This viewer is a good place to do a final check on the placement of pads and traces, and it just so happens that on the right hand side of the image above we noticed that there is a trace that could be too close to the edge! We will go back and fix this before submitting our gerbers for fabrication.

Although the capacitors we are using are not polar components, the viewer seems to still indicate what would be the positive pad. For illustrative purposes, consider the screenshot below:
![Example of Capacitor Placement](./resources/capacitor placement.png)

As you can see by the red dots indicated by JLC's viewer tool, if these were polar capacitors, those pads would be where the footprint location file thinks the anode (positive terminal) will be. If this does not match your circuit schematic, you would need to open up the .pos file in the **_assembly_** folder, and adjust the **Rotation** parameters accordingly.

And that's all there is to it! There are some steps you need to be careful to do properly, but overall JLCPCB makes custom PCB fabrication as straightforward as it could be. And for under $18 CAD they offer incredible value for their services, with fantastic customer service!

Check them out at https://jlcpcb.com/HAR! They even offer coupons for new users!!

Thanks JLCPCB for sponsoring our custom PCB fabrication  - we can't wait to work with you again!

## Authors and acknowledgment
This article was written by Owen Douglas and in full disclosure was created to fulfil a sponsorship agreement with JLCPCB.
